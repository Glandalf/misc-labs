import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Stats.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/auth',
    component: () => import('../views/Auth.vue')
  },
  {
    path: '/me',
    component: () => import('../views/Profile.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

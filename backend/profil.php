<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Faire comprendre au navigateur ce qu'on lui répond :
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');

    echo json_encode({"login": "Cacahuète", "codeAdherent": "234564568456"});
